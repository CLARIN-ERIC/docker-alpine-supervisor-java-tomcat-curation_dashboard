# version 5.2.0

# version 5.1.1
- changes in underlying [clarin curation module](https://github.com/clarin-eric/clarin-curation-module/blob/master/CHANGES.md) project

# version 5.1.0
- changes in underlying [clarin curation module](https://github.com/clarin-eric/clarin-curation-module/blob/master/CHANGES.md) project