#!/bin/bash

source "${DATA_ENV_FILE:-copy_data.env.sh}"


init_data (){
    CURATION_GIT_DIRECTORY="$(pwd)/curation-src"
    
    cleanup_data

    if [ -z ${DEV_LOCATION} ]
    then     
        mkdir -p "${CURATION_GIT_DIRECTORY}"
        (
            TMP_TGZ=$(mktemp)
            cd "${CURATION_GIT_DIRECTORY}" && 
                wget -O "${TMP_TGZ}" "${CURATION_REPO_URL}/archive/${CURATION_BRANCH}.tar.gz" &&
                tar zxvf "${TMP_TGZ}" --strip-components 1 &&
                rm "${TMP_TGZ}"
        )
    else
        cp -r $DEV_LOCATION $CURATION_GIT_DIRECTORY
    fi
    
}

cleanup_data () {
    if [ -d "${CURATION_GIT_DIRECTORY}" ]; then
        echo "Cleaning up source directory ${CURATION_GIT_DIRECTORY}"
        rm -rf "${CURATION_GIT_DIRECTORY}"
    fi
}
