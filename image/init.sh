#!/bin/sh

_KEYSTORE_FILE="/cert/server.jks"
_DEFAULT_PASS="password"


if [ -z "${PASSWORD}" ]; then
   echo "Using default STORE_PASS: ${_DEFAULT_PASS}"
   PASSWORD="${_DEFAULT_PASS}"
else
   echo "Using supplied PASSWORD"
fi

#
# Generate SSL certificate if the keystore does not exist
#
if [ -f "${_KEYSTORE_FILE}" ]; then
   echo "Using existing ssl certificate from ${_KEYSTORE_FILE}"
else
   echo "Generating new ssl certificate in ${_KEYSTORE_FILE}"
   keytool -genkey \
      -keyalg RSA \
      -alias selfsigned \
      -keystore "${_KEYSTORE_FILE}" \
      -storepass "${PASSWORD}" \
      -validity 360 \
      -keysize 2048 \
      -noprompt \
      -dname "cn=clarin.eu, ou=CLARIN, o=CLARIN-ERIC, c=NL" \
      -keypass "${PASSWORD}"
   
   #Exit if keytool command failed
   rc=$?;
   if [[ $rc != 0 ]]; then
      echo "Failed to generate ssl certificate";
      exit $rc;
   fi
fi

wget -qO- https://github.com/clarin-eric/VLO-mapping/archive/master.tar.gz | tar xvz -C /app/curation-dashboard/conf --strip-components=1
